package pl.autokomis.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User extends AbstractEntity {

    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String email;
    @Column
    private String token;
    @Column
    private String removed;


    //Relations
    @OneToOne
    @JoinColumn(name = "id_person")
    private Person person;


    @ManyToMany
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<RoleType> roleType;

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Person getPerson() {
        return person;
    }

    public User setPerson(Person person) {
        this.person = person;
        return this;
    }

    public List<RoleType> getRoleType() {
        return roleType;
    }

    public User setRoleType(List<RoleType> roleType) {
        this.roleType = roleType;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getToken() {
        return token;
    }

    public User setToken(String token) {
        this.token = token;
        return this;
    }

    public String getRemoved() {
        return removed;
    }

    public User setRemoved(String removed) {
        this.removed = removed;
        return this;
    }
}
