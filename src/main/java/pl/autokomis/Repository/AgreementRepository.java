package pl.autokomis.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.Agreement;
import pl.autokomis.Entity.AgreementType;

import java.util.List;

@Repository
public interface AgreementRepository extends JpaRepository<Agreement, Integer> {


    //Search all agreement by agreementType;
    @Query("FROM Agreement a WHERE a.agreementType =:agreementType")
    List<Agreement> findAllByAgreementType(@Param("agreementType")AgreementType purchaseType);


    //Search agreement by car id;
    @Query("FROM Agreement a WHERE a.car.id =:id")
    Agreement findAgreementByIdCars(@Param("id") Integer id);




}
