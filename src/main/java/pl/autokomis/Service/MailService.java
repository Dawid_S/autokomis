package pl.autokomis.Service;

import pl.autokomis.Dto.UserDto;
import pl.autokomis.Entity.Email;

public interface MailService {

    void sendMail(UserDto userDto, Email type, String token);

    String getToken();

}
