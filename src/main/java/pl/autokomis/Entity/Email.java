package pl.autokomis.Entity;


public enum Email {
    ACCOUNT_CONFIRMATION("confirmation", "Rejestracja - link aktywacyjny");


    private String urlType;
    private String sub;

    Email(String urlType, String sub) {
        this.urlType = urlType;
        this.sub = sub;
    }

    public String getUrlType() {
        return urlType;
    }

    public Email setUrlType(String urlType) {
        this.urlType = urlType;
        return this;
    }

    public String getSub() {
        return sub;
    }

    public Email setSub(String sub) {
        this.sub = sub;
        return this;
    }
}

