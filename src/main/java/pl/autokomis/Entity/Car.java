package pl.autokomis.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "car")
public class Car extends AbstractEntity{

    @Column
    private String vin;
    @Column
    private String year;
    @Column
    private String mark;
    @Column
    private String model;
    @Column
    private String liabilityNumber;
    @Column
    private String registartionNumber;
    @Column
    private String fuelType;
    @Column
    private String engine;
    @Column
    private int horsePower;
    @Column
    private String gearBox;
    @Column
    private String description;
    @Column
    private int distance;


    //Relations
    @ManyToOne
    @JoinColumn(name = "person_id")
    protected Person owner;

    @OneToOne
    @JoinColumn(name = "car_type_id")
    private CarType carType;

    @OneToMany(mappedBy = "car")
    private List<Agreement> agreements;


    public Car() {
    }

    public String getVin() {
        return vin;
    }

    public Car setVin(String vin) {
        this.vin = vin;
        return this;
    }

    public String getYear() {
        return year;
    }

    public Car setYear(String year) {
        this.year = year;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public Car setMark(String mark) {
        this.mark = mark;
        return this;
    }

    public String getModel() {
        return model;
    }

    public Car setModel(String model) {
        this.model = model;
        return this;
    }

    public String getLiabilityNumber() {
        return liabilityNumber;
    }

    public Car setLiabilityNumber(String liabilityNumber) {
        this.liabilityNumber = liabilityNumber;
        return this;
    }

    public String getRegistartionNumber() {
        return registartionNumber;
    }

    public Car setRegistartionNumber(String registartionNumber) {
        this.registartionNumber = registartionNumber;
        return this;
    }

    public String getFuelType() {
        return fuelType;
    }

    public Car setFuelType(String fuelType) {
        this.fuelType = fuelType;
        return this;
    }

    public String getEngine() {
        return engine;
    }

    public Car setEngine(String engine) {
        this.engine = engine;
        return this;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public Car setHorsePower(int horsePower) {
        this.horsePower = horsePower;
        return this;
    }

    public String getGearBox() {
        return gearBox;
    }

    public Car setGearBox(String gearBox) {
        this.gearBox = gearBox;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Car setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getDistance() {
        return distance;
    }

    public Car setDistance(int distance) {
        this.distance = distance;
        return this;
    }

    public Person getOwner() {
        return owner;
    }

    public Car setOwner(Person owner) {
        this.owner = owner;
        return this;
    }

    public CarType getCarType() {
        return carType;
    }

    public Car setCarType(CarType carType) {
        this.carType = carType;
        return this;
    }

    public List<Agreement> getAgreements() {
        return agreements;
    }

    public Car setAgreements(List<Agreement> agreements) {
        this.agreements = agreements;
        return this;
    }
}
