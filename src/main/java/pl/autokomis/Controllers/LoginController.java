package pl.autokomis.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.autokomis.Dto.UserDto;
import pl.autokomis.Entity.Email;
import pl.autokomis.Service.MailService;
import pl.autokomis.Service.UserService;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private MailService mailService;

    public final String LOGIN_PAGE = "Login/login";
    public final String REGISTRATION_PAGE = "Login/registration";
    public final String USER_INFO_PAGE = "User/addUser";

    @GetMapping("/login")
    public String getLoginPage(ModelMap modelMap){
        return LOGIN_PAGE;
    }

    @PostMapping("/login")
    public String getPage(ModelMap modelMap, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            modelMap.put("message", "Niepoprawna nazwa lub hasło");
            System.out.println("tutaj");
            return LOGIN_PAGE;
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        if(userService.checkedUserAccount(username)){
           return  USER_INFO_PAGE;
        }
        return "redirect:/carsList";
    }


    /**
     * Method getRegistration - show a registration form.
     * @return registration form
     * */
    @GetMapping("/registration")
    public String getRegistration(ModelMap modelMap){
        modelMap.put("userForm", new UserDto());
        return REGISTRATION_PAGE;
    }



    /**
     * Method saveNewUser - add user. Check a username and email with the information in database.
     * @return return login page/ registration page (if something wrong)
     * */
    @PostMapping("/registration")
    public String saveNewUser(@ModelAttribute("userForm") UserDto userDto, ModelMap modelMap, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/registration";
        }

        if(!userService.getUsersChecked(userDto.getUsername()) && !userService.getUserMailChecked(userDto.getEmail())){
            try{
                String token = mailService.getToken();
                userService.saveUser(userDto, token);
                mailService.sendMail(userDto, Email.ACCOUNT_CONFIRMATION, token);
                return LOGIN_PAGE;
            } catch (MailException e){System.out.println("Błąd w wysyłaniu wiadomości mail");}
        }else{

            if(userService.getUserMailChecked(userDto.getEmail())==true && userService.getUsersChecked(userDto.getUsername()) == true){
                modelMap.put("message", "Nazwa użytkownika zajęta. Proszę wybrać ponownie");
                modelMap.put("message2", "Email istnieje w bazie");
                return REGISTRATION_PAGE;
            } else if(userService.getUserMailChecked(userDto.getEmail())==true){
                modelMap.put("message2", "Email istnieje w bazie");
                return REGISTRATION_PAGE;
            } else if(userService.getUsersChecked(userDto.getUsername()) == true){
                modelMap.put("message", "Nazwa użytkownika zajęta. Proszę wybrać ponownie");
                return REGISTRATION_PAGE;
            }

        }
        return "redirect:/registration";
    }


    /**
     * Method confirmAcount - confirmation a account.
     * @return return login page
     * */
    @RequestMapping("/confirmation")
    public String confirmAcount(@RequestParam("token") String token, ModelMap modelMap){
        userService.confirmAccount(token);
        return "redirect:/login";
    }


}
