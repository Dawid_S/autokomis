package pl.autokomis.Entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
abstract class AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date added;

    public Integer getId() {
        return id;
    }

    public AbstractEntity setId(Integer id) {
        this.id = id;
        return this;
    }

    public Date getAdded() {
        return added;
    }

    public AbstractEntity setAdded(Date added) {
        this.added = added;
        return this;
    }

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id=" + id +
                ", added=" + added +
                '}';
    }
}
