package pl.autokomis.Service;

import pl.autokomis.Dto.PersonDto;
import pl.autokomis.Dto.UserDto;

import java.util.List;


public interface UserService {

    List<PersonDto> getPersonList();

    void saveUser(UserDto userDto, String token);

    boolean getUsersChecked( String userDtoUsername);

     boolean getUserMailChecked(String userDtoMail);

     void confirmAccount(String token);

     List<UserDto> getUserListInfo();

     UserDto getUserById(Integer id);

    void deleteUserById(Integer id);

    void blockUserById(Integer id);

    boolean checkedUserAccount(String username);

}
