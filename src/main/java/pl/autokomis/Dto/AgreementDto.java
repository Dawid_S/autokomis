package pl.autokomis.Dto;
import java.math.BigDecimal;
import java.util.Date;


public class AgreementDto {


    private Integer id;
    private Date added;
    private String description;
    private BigDecimal amount;
    private Integer agreementType;
    private Integer car;
    private Integer person;


    public AgreementDto() {
    }

    public Integer getId() {
        return id;
    }

    public AgreementDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public Date getAdded() {
        return added;
    }

    public AgreementDto setAdded(Date added) {
        this.added = added;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public AgreementDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public AgreementDto setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Integer getAgreementType() {
        return agreementType;
    }

    public AgreementDto setAgreementType(Integer agreementType) {
        this.agreementType = agreementType;
        return this;
    }

    public Integer getCar() {
        return car;
    }

    public AgreementDto setCar(Integer car) {
        this.car = car;
        return this;
    }

    public Integer getPerson() {
        return person;
    }

    public AgreementDto setPerson(Integer person) {
        this.person = person;
        return this;
    }
}
