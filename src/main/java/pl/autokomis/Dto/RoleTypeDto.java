package pl.autokomis.Dto;
public class RoleTypeDto {

    private Integer id;
    private String name;
    private boolean isActive;

    public RoleTypeDto() {
    }

    public Integer getId() {
        return id;
    }

    public RoleTypeDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RoleTypeDto setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public RoleTypeDto setActive(boolean active) {
        isActive = active;
        return this;
    }
}
