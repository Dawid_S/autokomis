package pl.autokomis.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.RoleType;

@Repository
public interface RoleTypeRepository extends JpaRepository<RoleType, Integer>{

    RoleType findByName(String typName);

}
