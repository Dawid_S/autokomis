package pl.autokomis.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.autokomis.Dto.PersonDto;
import pl.autokomis.Dto.RoleTypeDto;
import pl.autokomis.Dto.UserDto;
import pl.autokomis.Service.PersonService;
import pl.autokomis.Service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PersonService personService;

    public final String USER_ADD = "User/addUser";

    /**
     * Method addUser - get user form.
     * @return  user form
     * */
    @GetMapping("/addUser")
    public String addUser(ModelMap modelMap){
        modelMap.addAttribute("personForm", new PersonDto());
        return USER_ADD;
    }

    /**
     * Method saveUser - add user to database.
     * @return  user list
     * */
    @PostMapping("/addUser")
    public String saveUser(@ModelAttribute("personForm") PersonDto personDto, ModelMap modelMap){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        personService.savePersonDetails(personDto, username);
        return "redirect:/carsList";
    }






}
