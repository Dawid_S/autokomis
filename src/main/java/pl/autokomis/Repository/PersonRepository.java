package pl.autokomis.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

    Person findByUserId(Integer id);
}
